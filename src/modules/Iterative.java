package modules;
import models.List;
/**
 * Iterative method
 */
public class Iterative {
    /**
     * Constructor
     */
    public Iterative() {

    }
    /**
     * Gets the second element (irerative)
     * @param list List to search in (List)
     * @return The second element from the list (Element)
     */
    public int findSecond(List list){
        int max = 0, second = 0;
        int size = list.size();
        for (int i = 0; i < size; i++){
            if(list.get(i).getVal() >= max) {
                second = max;
                max = list.get(i).getVal();
            }
            else if(list.get(i).getVal() > second) {
                second = list.get(i).getVal();
            }
        }
        return second;
    }
}

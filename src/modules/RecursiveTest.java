package modules;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import models.Element;
import models.List;

class RecursiveTest {
    List list;

    @BeforeEach
    void setUp() {
        list = new List(new Element(3), new Element(6));
        list.add(new Element(2));
        list.add(new Element(1));
    }

    @org.junit.jupiter.api.Test
    void findSecond() {
        Recursive recursive = new Recursive();
        assertEquals(3, recursive.findSecond(list),
                "Second element ok.");
    }
}
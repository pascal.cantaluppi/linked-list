package modules;
import models.Element;
import models.List;
/**
 * Recursive method
 */
public class Recursive {
    /**
     * Constructor
     */
    public Recursive() {

    }
    private int max = 0, second = 0;
    /**
     * Gets the second element (recursive)
     * @param list List to search in (List)
     * @return The second element from the list (Element)
     */
    public int findSecond(List list){
        Element element = list.get(0);
        return recursion(element);
    }
    private int recursion(Element element) {
        if(element.getVal() >= max){
            second = max;
            max = element.getVal();
        }
        else if(element.getVal() > second) {
            second = element.getVal();
        }
        if(element.getNext() != null) {
            recursion(element.getNext());
        }
        return second;
    }
}

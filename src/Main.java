import models.List;
import models.Element;
import modules.Iterative;
import modules.Recursive;

/**
 * Data structures and algorithms - Linked List
 *
 * @author Pascal Cantaluppi
 * @version 1.0
 */
public class Main {
    /**
     * The main class launches the application.
     *
     * @param args command line parameters
     */
    public static void main(String args[]){
        List list = new List(new Element(3), new Element(6));
        list.add(new Element(2));
        list.add(new Element(1));
        System.out.println("Size of list: " + list.size());
        Iterative iterative = new Iterative();
        System.out.println("Second biggest value (iterative): " + iterative.findSecond(list));
        Recursive recursive = new Recursive();
        System.out.println("Second biggest value (recursive): " + recursive.findSecond(list));
    }
}

package models;
/**
 * Model of an element
 */
public class Element {
    int val;
    Element prev;
    Element next;
    /**
     * Constructor
     * @param val Value of the element
     */
    public Element(int val){
        this.val = val;
    }
    /**
     * Gets the current value
     * @return Current value (int)
     */
    public int getVal() {
        return val;
    }
    /**
     * Sets the current value
     * @param val The new value
     */
    public void setVal(int val) {
        this.val = val;
    }
    /**
     * Gets the previous element
     * @return Previous element (Element)
     */
    public Element getPrev() {
        return prev;
    }
    /**
     * Sets the previous element
     * @param prev The previous element
     */
    public void setPrev(Element prev) {
        this.prev = prev;
    }
    /**
     * Gets the next element
     * @return Previous next (Element)
     */
    public Element getNext() {
        return next;
    }
    /**
     * Sets the next element
     * @param next The next element
     */
    public void setNext(Element next) {
        this.next = next;
    }
}

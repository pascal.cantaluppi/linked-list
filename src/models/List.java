package models;
/**
 * Model of a list
 */
public class List {
    Element first;
    Element last;
    /**
     * Constructor
     * @param first First element (Element)
     * @param last Last element (Element)
     */
    public List(Element first, Element last){
        this.first = first;
        this.last = last;
        this.first.prev = null;
        this.first.next = this.last;
        this.last.prev = this.first;
        this.last.next = null;
    }
    /**
     * Adds a new element
     * @param element The new element (Element)
     */
    public void add(Element element){
        last.next = element;
        element.prev = last;
        last = element;
    }
    /**
     * Gets an element
     * @param index The index of the element (int)
     * @return The element from the list (Element)
     */
    public Element get(int index){
        Element element = first;
        for(int i = 0; i < index; i++){
            element = element.next;
        }
        return element;
    }
    /**
     * Gets the length of the list
     * @return List size (int)
     */
    public int size(){
        int i = 0;
        Element element = first;
        while (element != last){
            i++;
            element = element.next;
        }
        return i+1;
    }
}

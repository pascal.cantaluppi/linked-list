# Data structures and algorithms

## Linked List

### What's that all about ?

In computer science, a linked list is a linear collection of data elements whose order is not given by their physical placement in memory. Instead, each element points to the next. It is a data structure consisting of a collection of nodes which together represent a sequence.

<img src="https://gitlab.com/pascal.cantaluppi/linked-list/-/raw/main/public/linked-list.png" />
